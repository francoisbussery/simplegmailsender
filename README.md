# Simple Gmail Email Sender #

Use Gmail API to send an email with an attached image.
This tool is based on gmail API + mime-js
(https://github.com/ikr0m/mime-js/)


### How to set up credencials ###
Go to google console to fetch clientid & apikey
https://console.developers.google.com

inside index.html, set the api key & client key
var clientId='xxx.apps.googleusercontent.com';
var keyId='xxx'
    

### How to use it ###

Key client id & apikey from google 

to test it: 
python -m SimpleHTTPServer 8000

