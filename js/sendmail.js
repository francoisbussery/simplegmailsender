
      var clientId='';
      var apiKey=''
      var scopes =  'https://www.googleapis.com/auth/gmail.send';
      var gmailReady=false;
      function InitGmailSender(MyclientId,MyapiKey) {
     //   clientId=MyclientId;
      //  apiKey=MyapiKey
      }

      function handleClientLoad() {
        gapi.client.setApiKey(apiKey);
        window.setTimeout(checkAuth, 1);
      }
 

      
      function checkAuth() {
        gapi.auth.authorize({
          client_id: clientId,
          scope: scopes,
          immediate: true
        }, handleAuthResult);
      }

      function handleAuthClick() {
        gapi.auth.authorize({
          client_id: clientId,
          scope: scopes,
          immediate: false
        }, handleAuthResult);
        return false;
      }

      function handleAuthResult(authResult) {
        if(authResult && !authResult.error) {
          loadGmailApi();
        } else {
        handleAuthClick();
        }
      }

      function loadGmailApi() {
        gapi.client.load('gmail', 'v1',loadGmailApiCb);
      }


function loadGmailApiCb()
{
   gmailReady=true;
   console.log("ok");
}


      function sendEmail(message,callback)
      {
        if (gmailReady==true)
        {
          var mimeTxt = Mime.toMimeTxt(message);
        
          sendMessage(
              mimeTxt,callback
          );
        }
        else
        {
           console.log("email not sent. gmail not ready");
        }
        return false;
      }


      function sendMessage(headers_obj, callback)
      {
       // console.log(headers_obj);
        var sendRequest = gapi.client.gmail.users.messages.send({
          'userId': 'me',
          'resource': {
            'raw': window.btoa(headers_obj).replace(/\+/g, '-').replace(/\//g, '_')
          }
        });

        return sendRequest.execute(callback);
      }
